# YAML::Formation - AWS Cloudformation Templates in YAML
* * *

I compiled this utility to allow me to describe AWS Cloudformation templates in the YAML markup.

It saves time when you dont have to consider 'opening' or 'closing' braces, double quoting everything, list and list element syntax.

Your YAML must follow its rules too, but you only have to use the shift key for 1 of its specifiers (:), this blows json out of the water when you are authoring templates.

The flipside of this is that current Cloudformation templates can be decompiled into YAML. In theory any valid json schema can.

*YAML*
```
List1:
- Listitem1: 'one'
- Listitem2: 'two'
```
*JSON*
```
{"List1":[{"Listitem1":"one"},{"Listitem2":"two"}]}
```

You dont have to scan for open/close syntax when you read or write the YAML. 

All those brackets and quotes need an extra 'shift' keypress so require more time at the keyboard.

These differences become more and more pronouced as the complexity of your template increases.

See the end of this document for a quick 'Writing JSON in YAML' primer.

**Management and Administration**

These templates can get large and complex. Irresepective of the markup the template specifications need to be broken down into smaller chunks to allow for easier debugging and extension.

This utility should be run from the directory in your source control which will hold the YAML specifications for the template you are working on. It will work like 'bundler' or 'maven' or 'npm'. 

Mappings, Outputs, Parameters, and Resources are split into subdiretories below your template root. 

Any file with a .yml extension in these directories will be assembled into the output json. These files have no naming constraints, can contain one or many specifications (be they mappings, outputs, etc...) and must conform to the specification required by the Cloudformation template spec.

Files in the 'Outputs' folder will describe the child objects in the 'Outputs:' section of the json that is produced. The same is true for the other 3 sections. 

In the root of the template folder must exist a file called 'header.yml' and this will contain the following:

```
---
AWSTemplateFormatVersion: '2010-09-09'
Description: 'Some template'
```

**Why do it this way?**

When split out like this several things are possible which aid in fast and efficient debugging and creation that are normally not possible when dealing with the entire document, debugging can be done on a single element

There are many ways to get fast indication about where the bad syntax in your json is. The problem is that what is reported by things like underscore, python's json.tool, or jsonlint.com for example can be inaccurate.

The position of your syntax error is not always where these tools will tell you that it is. This is especialy true for "[ ]" or "{ }", leaving a few of those off in your document can make the interpreting library start telling you that there are problems with parts of your json that are actually correct.

In its simplest extreme, each resource, mapping, parameter, or output can be in its own .yml file. debugging will be detailed later on but this utility includes functionality to convert json strings to YAML or vice versa from <STDIN>. 

This makes the process of debugging the json string thats coming from your YAML a piece of cake. It also allows you to look at a much smaller piece of the full template information.

It helps when it comes to extending the templates too. For example, I have a **.yml** file called **vpc0.yml** in my resources folder.

Inside there I have specs for a VPC, IGW, IGW Attachment, and a couple of subnets. I have named the resources in such a way that they can be unique-ified with simple regex.

So all I have to do, is go into the resources directory, copy the **vpc0.yml** file to a new file **vpc1.yml** and do my search and replace in vi. I can give the json a once over if I like:

```
$ cat resources/vpc1.yml | yfn y2j

{"VPC1PracSubnet":{"Type":"AWS::EC2::Subnet","Properties":{"VpcId":{"Ref":"VPC1"},"CidrBlock":{"Ref":"VPC1PracSubnetCIDR"},"Tags":[{"Value":"VPC1 Prac Subnet","Key":"Name"}]}},"NetAttach1":{"Type":"AWS::EC2::VPCGatewayAttachment","Properties":{"VpcId":{"Ref":"VPC1"},"InternetGatewayId":{"Ref":"IGW1"}}},"IGW1":{"Type":"AWS::EC2::InternetGateway","Properties":{"Tags":[{"Value":"CF SME Prac IGW1","Key":"Name"}]}},"VPC1":{"Type":"AWS::EC2::VPC","Properties":{"CidrBlock":{"Ref":"VPC1CIDR"},"Tags":[{"Value":"CF SME Prac VPC1","Key":"Name"}]}}}
```

Arguably the best thing here is that very little error checking is required. If you write bad YAML youll get a warning about which file(s) of yours contain the bad YAML and your json will not compile. Bad YAML is whatever the 'YAML::XS' module thinks is bad.

The constraints that the YAML markup puts on you more or less ensure that the json that results from it is going to be completely valid. 
 
# Getting Started:
* * *

### Requirements:

```
perl 5.8+
YAML::XS
JSON
```

### Installation:

Its best to install the OS specific versions of the 'cpanminus' application:

Amazon Linux:

```
$ sudo yum install perl-App-cpanminus
``` 

Debian based:

```
$ sudo apt-get install cpanminus
```

If your platform is missing some of the more important base perl modules like JSON and YAML this will be the fastest way for you to get them in, these packages will install a great deal faster than building straight with CPAN.

```
$ sudo cpanm install YAML::XS JSON
```

Still, if cpanminus is not available for you, the regular CPAN will do

```
$ sudo perl -MCPAN -e 'install YAML::XS' 
$ sudo perl -MCPAN -e 'install JSON' 
```

**Its best you put this stuff in place using sudo as shown above**

You can use 'local::lib' and install with a non privileged user, at the end of the day:

```
$ perl -e 'use JSON; use YAML::XS'
```
        
must not print an error. After this works you can clone this repository:

```
$ git clone https://github.com/lokerd/yaml-formation.git
```

add the bin folder in here to your path:

bash:

```
$ export PATH=$PATH:/path/to/yaml-formation/bin
```

fish:

```
$ set -gx /path/to/yaml-formation/bin $PATH
```

### Usage:
* * *
A valid directory structure and 'header.yml' will need to be present wherever you run this from.

| Assembly      |                                |
| ------------- | ------------------------------ |
| `ay`          | assemble and output YAML       |
| `aj`          | assemble and output JSON       |

Examples:

```
$ yfn aj > json.out
$ yfn ay > yml.out
```

`ay` is useful if you would like to see why `aj` is throwing an error. It will print the YAML array as it exists right before attempting the encoding.

| Debugging     |                                                        |
| ------------- | ------------------------------------------------------ |
| `y2j`         | takes YAML on <STDIN>, prints json string to <STDOUT>  |
| `j2y`         | takes json string on <STDIN>, prints YAML to <STDOUT>  |

Examples:

```
$ cat yml | yfn y2j
$ cat json | yfn j2y
```
`y2j` is primarily for testing small files. To make sure the JSON is to spec, cat the .yml file into this as shown above. 

| Conversion    |                                                                  |
| ------------- | ---------------------------------------------------------------- |
| `r1`          | create a single .yml file for each section and a header.yml file |
| `r2`          | create a single .yml file for each resource/output etc           |

This will take a CF template on STDIN and split it out into the structure expected by my utility. 

`r1` format is a bit easier to work with, you can make copies of the files and delete the irrelevant stuff from each file for a fast logical grouping.

`r2` can be useful if you need to add and remove single elements from the template, just delete the file, the files will be called the name of the resource they define:

Examples:

`r1`
```
$ cd < where you want to build files/dirs >

$ cat /path/to/some_template.json | yfn r1

$ cat Outputs/Outputs.yml

Target:
  Description: target for eip
  Value:
    Fn::Join:
    - ''
    - - Ref: TargetInstance
      - ' ( Private: '
      - Fn::GetAtt:
        - TargetInstance
        - PrivateIp
      - ' )'
Worker:
  Description: worker instance
  Value:
    Fn::Join:
    - ''
    - - Ref: WorkerInstance
      - ' ( Public: '
      - Fn::GetAtt:
        - WorkerInstance
        - PublicIp
      - ', Private:'
      - Fn::GetAtt:
        - WorkerInstance
        - PrivateIp
      - ' )'

$ cat Outputs/Outputs.yml | yfn y2j | underscore print

{
  "Target": {
    "Description": "target for eip",
    "Value": {
      "Fn::Join": [
        "",
        [
          { "Ref": "TargetInstance" },
          " ( Private: ",
          { "Fn::GetAtt": ["TargetInstance", "PrivateIp"] },
          " )"
        ]
      ]
    }
  },
  "Worker": {
    "Description": "worker instance",
    "Value": {
      "Fn::Join": [
        "",
        [
          { "Ref": "WorkerInstance" },
          " ( Public: ",
          { "Fn::GetAtt": ["WorkerInstance", "PublicIp"] },
          ", Private:",
          { "Fn::GetAtt": ["WorkerInstance", "PrivateIp"] },
          " )"
        ]
      ]
    }
  }
}
```

`r2`
```
$ cd < where you want to build files/dirs >

$ cat /path/to/some_template.json | yfn r2

$ cat Resources/VPC0.yml

VPC0:
  Type: AWS::EC2::VPC
  Properties:
    CidrBlock: 
      Ref: VPC0CIDR
    Tags:
    - Key: Name
      Value: CF SME Prac VPC0

$ cat Resources/VPC0.yml | yfn y2j | python -m json.tool

{
        "VPC0": {
                "Properties": {
                        "CidrBlock": {
                                "Ref": "VPC0CIDR"
                        },
                        "Tags": [{
                                "Key": "Name",
                                "Value": "CF SME Prac VPC0"
                        }]
                },
                "Type": "AWS::EC2::VPC"
        }
}
```

# Writing JSON in YAML
* * *

Fields are separated by the ':' like in json. Each step in the hierarchy must be indented by 2 spaces. 
```
Step1:
  Step2-1:
    Step3-1: value
  Step2-2:
    Step3-2:
      value

{
    "Step1": {
        "Step2-1": {
            "Step3-1": "value"
        },
        "Step2-2": {
            "Step3-2": "value"
        }
    }
}

```

The exception is when you are starting a list. You can think of the '-' charachter as the '[' open square bracket. It will be closed when you return 2 spaces in endentation at the end of the item and this one doesnt get indented:

```
List1:
- Listitem1: 'value'
- - Subitem1:
      'value'
  - Subitem2:
    - SubitemA: 'value'
    - SubitemB:
        'value'
- Listitem2:
    'value'

{
    "List1": [
        {
            "Listitem1": "value"
        },
        [
            {
                "Subitem1": "value"
            },
            {
                "Subitem2": [
                    {
                        "SubitemA": "value"
                    },
                    {
                        "SubitemB": "value"
                    }
                ]
            }
        ],
        {
            "Listitem2": "value"
        }
    ]
}
```
to the 'right' of the ':' there are only 4 possibilities:

- `string`
- `list`
- `boolean`
- `number`

Be careful not to include the important characters in `strings` and `lists`, the `boolean` and `number` dont need to be quoted:

```
Item: 100
Value: true
```
If you need to place an important piece of punctuation to the 'right' of the ':' - 

```
Example: 'I am a quoted string: i can put anything here -'
```
is fine but:
```
Example: I am a quoted string: i can put anything here -
```
is not valid YAML. Its safest to just quote anything you know is a string:
```
Example: this is ok theres no punctuation here

{"Example":"this is ok theres no punctuation here"}

Example1: 'single quote '' like that'

{"Example1":"single quote ' like that"}

Example2: '{ [ - ] } "'

{"Example2":"{ [ - ] } \""}
```

Items can cross lines:

```
Item: Statement which crosses lines

{"Item":"Statement which crosses lines"}

Item1:
  Statement which crosses lines

{"Item1":"Statement which crosses lines"}

Item2: Statement which 
  crosses lines

{"Item2":"Statement which crosses lines"}

```

A problem you are likely to face if planning to author yaml intended for a certain json schema is how you get the actual newline character through.

`Test: \n` -> `{"Test":"\\n"}`

`Test: \\n` -> `{"Test":"\\\\n"}`      

Its common to include newline characters in UserData, Init, and Join. It turns out that if you want "\n" to show in your json you must issue the yaml newline. 

This is hitting enter twice. It is not "\n\n". I found the best way around this is to use the newline as the Join argument where you would normally have used an empty string.

```
UserData:
  Fn::Base64:
    Fn::Join:
    - '

'
    - - '#!/usr/bin/env bash'
      - 'echo ''hi'''
      - '...'

{
        "UserData": {
                "Fn::Base64": {
                        "Fn::Join": [
                                "\n",
                                [
                                        "#!/usr/bin/env bash",
                                        "echo 'hi'",
                                        "..."
                                ]
                        ]
                }
        }
}
```
# Cloudformation Intrinsic Functions
* * *
*Condition*
```
NewVolume:
  DeletionPolicy: Snapshot
  Properties:
    AvailabilityZone:
      Fn::GetAtt:
      - Ec2Instance
      - AvailabilityZone
    Size:
      Fn::If:
      - CreateLargeSize
      - Ref: '100'
      - Ref: '10'
  Type: AWS::EC2::Volume
```
*Fn::Join*
```
Fn::Join:
- ''
- - Ref: TEST0Instance
  - ' ( Public: '
  - Ref: TEST0EIP 
  - ', Private:'
  - Fn::GetAtt:
    - TEST0Instance
    - PrivateIp
  - ' )'
```
*Fn::FindInMap*
```
myEC2Instance:
  Properties:
    ImageId:
      Fn::FindInMap:
      - RegionMap
      - Ref: AWS::Region
      - '32'
    InstanceType: m1.small
  Type: AWS::EC2::Instance
```
*Fn::GetAtt*
```
Fn::GetAtt:
- Resource
- Attribute
```
Heres a few examples. You can specify more than 1 resource/mapping/output/parameter per file, so you can group them into chunks which allow for easy extension:

*VPC base*
```
VPC0:
  Type: AWS::EC2::VPC
  Properties:
    CidrBlock: 
      Ref: VPC0CIDR
    Tags:
    - Key: Name
      Value: CF SME Prac VPC0
IGW0:
  Type: AWS::EC2::InternetGateway
  Properties:
    Tags:
    - Key: Name
      Value: CF SME Prac IGW0
NetAttach0:
  Type: AWS::EC2::VPCGatewayAttachment
  Properties:
    InternetGatewayId:
      Ref: IGW0
    VpcId:
      Ref: VPC0
VPC0PracSubnet:
  Type: AWS::EC2::Subnet
  Properties:
    CidrBlock:
      Ref: VPC0PracSubnetCIDR
    Tags:
    - Key: Name
      Value: VPC0 Prac Subnet
    VpcId:
      Ref: VPC0
```
*SNS/SQS and Policy*
```
Queue:
  Type: AWS::SQS::Queue
QueuePolicy:
  Type: AWS::SQS::QueuePolicy
  Properties:
    PolicyDocument:
      Id: CF SME Prac
      Statement:
      - Action: SQS:SendMessage
        Condition:
          ArnEquals:
            aws:SourceArn:
              Ref: QueueTopic
        Effect: Allow
        Principal:
          AWS: '*'
        Resource:
          Fn::GetAtt:
          - Queue
          - Arn
      Version: 2012-10-17
    Queues:
    - Ref: Queue
QueueTopic:
  Type: AWS::SNS::Topic
  Properties:
    Subscription:
    - Endpoint:
        Fn::GetAtt:
        - Queue
        - Arn
      Protocol: sqs
```
