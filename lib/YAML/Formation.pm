#==============================================================================
# Copyright 2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#==============================================================================
package YAML::Formation;
use strict;
use warnings;

use JSON;
use YAML::XS;
use Archive::Tar;
use vars qw|$config|;

# These are the directories we search for .yml
$config = [ 'Resources', 'Outputs', 'Mappings', 'Parameters' ];

# new( {} )             : Constructor
# ARGS                  : $args hash ref, placeholder
# RETURN                : blessed reference 
sub new 
{
    my ($class, $args) = @_;
    my $self->{_config} = $config; 
    $self->{_debug} = 0;
    bless ($self, $class);
    return $self; 
}

# enable_debug()        : switch on debug
# ARGS                  : NONE
# RETURN                : NONE 
sub enable_debug 
{
    my $self = shift;
    $self->{_debug} = 1;
    return $self;
}

# section( str [] [] )  : read yaml, push to yaml array, catch errors
# ARGS                  : $section - directory according to config hash
#                       : $yaml - yaml string array ref
#                       : $errors - errors array ref
# RETURN                : NONE
sub section 
{
        my ($self, $section, $yaml, $errors)  = @_; my @files;
        eval { @files = glob("$section/*.yml") }; 
        if (@files) { push(@{$yaml},"$section:\n") };    
        while (@files) 
        {
                my $file = pop @files; my @tmp;
                open FILE, $file or die "$file \n\n $!";
                while (<FILE>) 
                {
                        next if m(^---$);
                        push(@tmp, $_);
                }
                close FILE;
                eval { encode_json Load join('', @tmp) };
                if ($@) 
                { 
                        my $tmp = { file => $file, errors => $@ };
                        push(@{$errors}, $tmp);
                }
                else { for (@tmp) { push(@{$yaml}, "  $_") } }
        }
        return;
}

# assemble( str )       : Compile all .yml files to YAML/JSON
#                       : each section of config hash read
#                       : yaml array ref built, encoded to STDOUT
#                       : choosing out type yaml just dumps array ref
# ARGS                  : $out_type - output in 'yaml' or 'json'
# RETURN                : NONE
sub assemble 
{
        my ($self, $out_type) = @_; my $yaml = []; my $errors = [];
        open HEADER, 'header.yml' or die "- missing header.yml cwd -";
        while (<HEADER>) { push(@{$yaml},$_) }; close HEADER;
        for (@{$config}) { $self->section($_, $yaml, $errors) }
        if ($errors->[0]) { $self->print_errors($errors) }
        else
        {
                if ($out_type eq 'json') { print encode_json Load join('', @{$yaml}) }
                else { print @{$yaml} }
        }
}

# stdin( str )          : STDIN Processor
#                       : output to STDOUT unless 'rebase'
#                       : also has the hook for 'rebase'
# ARGS                  : $format - how to treat STDIN, default yaml
# RETURN                : NONE
sub stdin
{
        my ($self, $format) = @_; my $tmp = "";
        while (<>) 
        { 
                if ($format =~ m(json|rebase0|rebase1))
                { s/^\s+|\s+$//g and s/\\n//g }
                $tmp .= $_;
        }
        if ($format eq 'json') { print Dump decode_json $tmp }
        elsif ($format eq 'rebase0') { $self->rebase($tmp, 0) }
        elsif ($format eq 'rebase1') { $self->rebase($tmp, 1) }
        else { print encode_json Load $tmp }
}

# rebase( str int )     : Turns CF template to YAML files
#                       : will archive old file structure if present
#                       : buildout 0 - split per section 
#                       : eg. Parameters/Parameters.yml, Outputs/Outputs.yml etc
#                       : buildout 1 - split per resource 
#                       : eg. Parameters/Parameter1.yml, Parameters/Parameter2.yml, 
# ARGS                  : $json - JSON String
#                       : $buildout - buidout id 
# RETURN                : NONE
sub rebase {
        my ($self, $json, $buildout) = @_; my $j = decode_json($json);
        my @exists; if (-e 'header.yml') { push(@exists, "header.yml") } 
        for (@{$config}) 
        { 
                my @files; eval { @files = glob("$_/*") };
                push (@exists, @files) unless ($@);
        }
        if (@exists) 
        {
                my $bkup = "yfn-" . time . ".tgz";
                print "Might be overwriting stuff, Backing up old files:\n\n";
                Archive::Tar->create_archive($bkup, COMPRESS_GZIP, @exists);
                for (@exists) 
                { 
                        print "archived $_\n"; unlink($_) or die $!; 
                }
                print "\n$bkup built\n";
        }         
        open(my $header_fh, ">", "header.yml") or die "header.yml: $!";
        print $header_fh "---\n";
        for my $section (keys %{$j})
        {
                eval { keys $j->{$section} }; 
                if ($@) 
                {
                        print $header_fh "$section: \'$j->{$section}\'\n";
                        next;
                }
                unless(-e $section or mkdir $section) 
                { die "Unable to create $section : $!" }
                my $fh; if ($buildout == 0)
                { open($fh, ">", "$section/$section.yml") or die "$section/$section.yml: $!" }
                for my $key (keys $j->{$section}) 
                { 
                        if ($buildout == 1)
                        { open($fh, ">", "$section/$key.yml") or die "$key.yml: $!" }
                        my @yaml = split("\n", Dump $j->{$section}{$key}); shift @yaml;
                        print $fh "$key:\n"; for (@yaml) { print $fh "  $_\n" }; 
                        close($fh) if $buildout == 1;
                } 
                close($fh);
        }
}

# print_errors( str )   : prints contents of errors array
#                       : output to STDOUT
# ARGS                  : $errors - errors array ref
# RETURN                : NONE
sub print_errors 
{
    my ($self, $errors) = @_; 
    while (@{$errors}) 
    { 
            my $error = pop @{$errors}; 
            print "$error->{file} :: $error->{errors}\n"; 
    }
}

# debug( str )  : print timestamp and debug string
# ARGS          : $debug_string - string to be printed
#               : timestamped
# RETURNS       : NONE
sub debug 
{
    my $debug_string = shift;
    printf("%.3f :: %-12s\n", time, $debug_string);
}
